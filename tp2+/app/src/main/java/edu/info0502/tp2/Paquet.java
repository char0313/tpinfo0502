package edu.info0502.tp2;

import java.util.ArrayList;
import java.util.Collections;

public class Paquet {
	ArrayList<Carte> cartes;

	public Paquet() {
		cartes = new ArrayList();

		for (int valeur = 1; valeur <= 13; valeur++) {
			for (Carte.Enseigne enseigne: Carte.Enseigne.values()) {
				cartes.add(new Carte(valeur, enseigne));
			}
		}
	}

	public void melanger() {
		Collections.shuffle(cartes);
	}

	public int getNombreCartes() { return cartes.size(); }
	public Carte getCarte() { return cartes.remove(0); }

	public Carte[] getCartes(int nb_cartes) {
		Carte[] cartes_a_enlever = new Carte[nb_cartes];

		for (int i = 0; i < nb_cartes; i++) {
			cartes_a_enlever[i] = cartes.remove(0);
		}

		return cartes_a_enlever;
	}
}
