package edu.info0502.tp2;

import org.json.JSONObject;
import org.json.JSONArray;

public class Carte implements java.io.Serializable {
	static final long serialVersionUID = 1L;

	enum Enseigne {
		TREFLE,
		CARREAU,
		COEUR,
		PIQUE
	}

	int valeur;
	Enseigne enseigne;

	public Carte(int valeur, Enseigne enseigne) {
		this.valeur = valeur;
		this.enseigne = enseigne;
	}

	public Carte(JSONObject json) {
		valeur = json.getInt("valeur");

		if (json.getString("enseigne").equals("TREFLE")) {
			enseigne = Enseigne.TREFLE;
		} else if (json.getString("enseigne").equals("CARREAU")) {
			enseigne = Enseigne.CARREAU;
		} else if (json.getString("enseigne").equals("COEUR")) {
			enseigne = Enseigne.COEUR;
		} else if (json.getString("enseigne").equals("PIQUE")) {
			enseigne = Enseigne.PIQUE;
		}
	}

	public int getValeur() { return valeur; }
	public Enseigne getEnseigne() { return enseigne; }

	public JSONObject getJSONObject() {
		JSONObject json = new JSONObject();
		json.put("valeur", valeur);
		json.put("enseigne", enseigne);
		return json;
	}

	public String toString() {
		String repr = "";

		if (valeur > 1 && valeur <= 10) {
			repr += valeur;
		} else {
			switch (valeur) {
				case 1:
					repr += "As";
					break;
				case 11:
					repr += "Valet";
					break;
				case 12:
					repr += "Dame";
					break;
				case 13:
					repr += "Roi";
			}
		}

		repr += " de ";
		repr += enseigne.name();
		
		return repr;
	}
}
