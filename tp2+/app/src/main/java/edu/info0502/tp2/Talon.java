package edu.info0502.tp2;

import java.util.ArrayList;

public class Talon {
	ArrayList<Paquet> paquets;

	public Talon(int nb_paquet) {
		paquets = new ArrayList();

		for (int i = 0; i < nb_paquet; i++) {
			paquets.add(new Paquet());
		}
	}

	public void melanger() {
		for (Paquet paquet: paquets) {
			paquet.melanger();
		}
	}

	public int getNombrePaquet() {
		return paquets.size();
	}

	public Paquet getPaquet() {
		return paquets.get(0);
	}


	public Carte getCarte() {
		if (paquets.get(0).getNombreCartes() == 0) {
			paquets.remove(0);
		}
		return paquets.get(0).getCarte();
	}

	public Carte[] getCartes(int nb_cartes) {
		if (paquets.get(0).getNombreCartes() < nb_cartes) {
			paquets.remove(0);
		}
		return paquets.get(0).getCartes(nb_cartes);
	}
}
