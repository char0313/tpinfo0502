package edu.info0502.tp2;

public class App {
	public static void main(String[] args) {
		Talon t = new Talon(4);
		t.melanger();

		Carte[][] joueurs = new Carte[4][5];
		MainHelper.Combinaison[] joueurs_combinaison = new MainHelper.Combinaison[4];

		// Distribution des cartes
		for (int tour = 0; tour < 5; tour++) {
			for (Carte[] main_joueur: joueurs) {
				main_joueur[tour] = t.getCarte();
			}
		}

		// Affichage des combinaisons des mains
		for (int a = 0; a < 4; a++) {
			joueurs_combinaison[a] = MainHelper.evaluer(joueurs[a]);
			System.out.println(
					MainHelper.mainToString(joueurs[a])
					+ joueurs_combinaison[a]);
		}

		// Trouver le gagnant
		MainHelper.Combinaison gagnant = joueurs_combinaison[0];
		for (MainHelper.Combinaison combinaison: joueurs_combinaison) {
			MainHelper.Combinaison nouveau_gagnant = MainHelper.comparer(gagnant, combinaison);
			if (nouveau_gagnant != null) {
				gagnant = nouveau_gagnant;
			}
		}
		System.out.println("\nMain gagnante: " + gagnant + "\n");

		// Partie avec le Texas hold'em
		Carte[][] joueurs2 = new Carte[4][2];
		MainHelper.Combinaison[] joueurs_combinaison2 = new MainHelper.Combinaison[4];
		Carte[] cartes_visibles = new Carte[5];

		// Distribution des cartes
		for (int tour = 0; tour < 2; tour++) {
			for (Carte[] main_joueur: joueurs2) {
				main_joueur[tour] = t.getCarte();
			}
		}
		cartes_visibles = t.getCartes(5);

		// Affichage des cartes visibles
		System.out.println("Cartes vivibles: " + MainHelper.mainToString(cartes_visibles));

		// Affichage des meilleures combinaisons des mains
		for (int b = 0; b < 4; b++) {
			Carte[] meilleur_main = null;
			MainHelper.Combinaison meilleur_combinaison = null;
			for (int i = 0; i < 3; i++) {
            			for (int j = i + 1; j < 4; j++) {
                			for (int k = j + 1; k < 5; k++) {
						Carte[] main = new Carte[5];
						main[0] = joueurs2[b][0];
						main[1] = joueurs2[b][1];

						main[2] = cartes_visibles[i];
						main[3] = cartes_visibles[j];
						main[4] = cartes_visibles[k];

						MainHelper.Combinaison combinaison = MainHelper.evaluer(main);

						if (meilleur_main == null) {
							meilleur_main = main;
							meilleur_combinaison = combinaison;
						} else {
						}
					}
				}
			}

			joueurs_combinaison2[b] = meilleur_combinaison;
			System.out.println(MainHelper.mainToString(meilleur_main) + " " + meilleur_combinaison);
		}

		// Trouver le gagnant
		MainHelper.Combinaison gagnant2 = joueurs_combinaison2[0];
		for (MainHelper.Combinaison combinaison: joueurs_combinaison2) {
			MainHelper.Combinaison nouveau_gagnant2 = MainHelper.comparer(gagnant2, combinaison);
			if (nouveau_gagnant2 != null) {
				gagnant2 = nouveau_gagnant2;
			}
		}
		System.out.println("\nMain gagnante: " + gagnant2 + "\n");
	}
}
