package edu.info0502.tp2;

import java.util.ArrayList;

public class MainHelper {
	public static class Combinaison {
		enum TypeCombinaison {
			QUINTE_FLUSH_ROYALE(1),
			QUINTE_FLUSH(2),
			CARRE(3),
			FULL_HOUSE(4),
			COULEUR(5),
			QUINTE(6),
			BRELAN(7),
			DEUX_PAIRES(8),
			PAIRE(9),
			CARTE_HAUTE(10);

			final int rang;

			TypeCombinaison(int rang) {
				this.rang = rang;
			}

			public int getRang() { return rang; }
		}

		TypeCombinaison type;
		ArrayList<Integer> valeurs;
		Carte[] main;

		public Combinaison(TypeCombinaison type, ArrayList<Integer> valeurs, Carte[] main) {
			this.type = type;
			this.valeurs = valeurs;
			this.main = main;
		}

		public int getRang() { return type.getRang(); }
		public TypeCombinaison getType() { return type; }
		public ArrayList<Integer> getValeurs() { return valeurs; }
		public Carte[] getMain() { return main; };

		public String toString() {
			return type.name() + " valeurs: " + valeurs;
		}
	}

	public static String mainToString(Carte[] main) {
		String main_repr = "";
		for (Carte carte: main) {
			main_repr += "(" + carte + ") ";
		}
		return main_repr;
	}

	public static Combinaison evaluer(Carte[] main) {
		Combinaison.TypeCombinaison type;
		ArrayList<Integer> valeurs = new ArrayList();

		if (main[0].getValeur() == 10
				&& main[1].getValeur() == 11
				&& main[2].getValeur() == 12
				&& main[3].getValeur() == 13
				&& main[4].getValeur() == 1
				&& main[0].getEnseigne() == main[1].getEnseigne()
				&& main[1].getEnseigne() == main[2].getEnseigne()
				&& main[2].getEnseigne() == main[3].getEnseigne()
				&& main[3].getEnseigne() == main[4].getEnseigne()) {
			type = Combinaison.TypeCombinaison.QUINTE_FLUSH_ROYALE;
		} else if (main[0].getValeur() + 1 == main[1].getValeur()
				&& main[1].getValeur() + 1 == main[2].getValeur()
				&& main[2].getValeur() + 1 == main[3].getValeur()
				&& main[3].getValeur() + 1 == main[4].getValeur()
				&& main[0].getEnseigne() == main[1].getEnseigne()
				&& main[1].getEnseigne() == main[2].getEnseigne()
				&& main[2].getEnseigne() == main[3].getEnseigne()
				&& main[3].getEnseigne() == main[4].getEnseigne()) {
			type = Combinaison.TypeCombinaison.QUINTE_FLUSH;
			valeurs.add(main[0].getValeur());
		} else if (main[1].getValeur() == main[2].getValeur()
				&& main[2].getValeur() == main[3].getValeur()
				&& (main[0].getValeur() == main[1].getValeur()
					|| main[3].getValeur() == main[4].getValeur())) {
			type = Combinaison.TypeCombinaison.CARRE;
			valeurs.add(main[2].getValeur());
		} else if (main[0].getValeur() == main[1].getValeur()
				&& main[1].getValeur() == main[2].getValeur()
				&& main[3].getValeur() == main[4].getValeur()) {
			type = Combinaison.TypeCombinaison.FULL_HOUSE;
			valeurs.add(main[0].getValeur());
			valeurs.add(main[4].getValeur());
		} else if (main[0].getValeur() == main[1].getValeur()
				&& main[2].getValeur() == main[3].getValeur()
				&& main[3].getValeur() == main[4].getValeur()) {
			type = Combinaison.TypeCombinaison.FULL_HOUSE;
			valeurs.add(main[4].getValeur());
			valeurs.add(main[0].getValeur());
		} else if (main[0].getEnseigne() == main[1].getEnseigne()
				&& main[1].getEnseigne() == main[2].getEnseigne()
				&& main[2].getEnseigne() == main[3].getEnseigne()
				&& main[3].getEnseigne() == main[4].getEnseigne()) {
			type = Combinaison.TypeCombinaison.COULEUR;
		} else if (main[0].getValeur() + 1 == main[1].getValeur()
				&& main[1].getValeur() + 1 == main[2].getValeur()
				&& main[2].getValeur() + 1 == main[3].getValeur()
				&& main[3].getValeur() + 1 == main[4].getValeur()) {
			type = Combinaison.TypeCombinaison.QUINTE;
			valeurs.add(main[0].getValeur());
		} else if (main[0].getValeur() == main[1].getValeur()
				&& main[1].getValeur() == main[2].getValeur()) {
			type = Combinaison.TypeCombinaison.BRELAN;
			valeurs.add(main[0].getValeur());
		} else if (main[1].getValeur() == main[2].getValeur()
				&& main[2].getValeur() == main[3].getValeur()) {
			type = Combinaison.TypeCombinaison.BRELAN;
			valeurs.add(main[1].getValeur());
		} else if (main[2].getValeur() == main[3].getValeur()
				&& main[3].getValeur() == main[4].getValeur()) {
			type = Combinaison.TypeCombinaison.BRELAN;
			valeurs.add(main[2].getValeur());
		} else if ((main[0].getValeur() == main[1].getValeur()
					&& (main[2].getValeur() == main[3].getValeur()
						|| main[3].getValeur() == main[4].getValeur()))
				|| (main[1].getValeur() == main[2].getValeur()
					&& main[3].getValeur() == main[4].getValeur())) {
			type = Combinaison.TypeCombinaison.DEUX_PAIRES;
			valeurs.add(main[1].getValeur());
			valeurs.add(main[3].getValeur());
		} else if (main[0].getValeur() == main[1].getValeur()) {
			type = Combinaison.TypeCombinaison.PAIRE;
			valeurs.add(main[0].getValeur());
		} else if (main[1].getValeur() == main[2].getValeur()) {
			type = Combinaison.TypeCombinaison.PAIRE;
			valeurs.add(main[1].getValeur());
		} else if (main[2].getValeur() == main[3].getValeur()) {
			type = Combinaison.TypeCombinaison.PAIRE;
			valeurs.add(main[2].getValeur());
		} else if (main[3].getValeur() == main[4].getValeur()) {
			type = Combinaison.TypeCombinaison.PAIRE;
			valeurs.add(main[3].getValeur());
		} else {
			int valeur_max = main[0].getValeur();

			for (Carte carte: main) {
				if (carte.getValeur() == 1) {
					valeur_max = carte.getValeur();
					break;
				} else if (carte.getValeur() > valeur_max) {
					valeur_max = carte.getValeur();
				}
			}

			type = Combinaison.TypeCombinaison.CARTE_HAUTE;
			valeurs.add(valeur_max);
		}

		return new Combinaison(type, valeurs, main);
	}

	public static Combinaison evaluer(Carte[] main, Carte[] flop, Carte turn, Carte river) {
		Carte[] cartes_visibles = new Carte[5];
		cartes_visibles[0] = flop[0];
		cartes_visibles[1] = flop[1];
		cartes_visibles[2] = flop[2];

		cartes_visibles[3] = turn;
		cartes_visibles[4] = river;

		MainHelper.Combinaison meilleur_combinaison = null;

		for (int i = 0; i < 3; i++) {
            for (int j = i + 1; j < 4; j++) {
                for (int k = j + 1; k < 5; k++) {
					Carte[] main_temp = new Carte[5];
					main_temp[0] = main[0];
					main_temp[1] = main[1];

					main_temp[2] = cartes_visibles[i];
					main_temp[3] = cartes_visibles[j];
					main_temp[4] = cartes_visibles[k];

					MainHelper.Combinaison combinaison = MainHelper.evaluer(main_temp);

					if (meilleur_combinaison == null) {
						meilleur_combinaison = combinaison;
					} else {
						if (comparer(meilleur_combinaison, combinaison) != null) { // on ne gère pas les églités, trop complexe
							meilleur_combinaison = comparer(meilleur_combinaison, combinaison);
						}
					}
				}
			}
		}

		return meilleur_combinaison;
	}

	public static Combinaison comparer(Combinaison a, Combinaison b) {
		if (a.getRang() < b.getRang()) {
			return a;
		} else if (b.getRang() < a.getRang()) {
			return b;
		} else {
			switch (a.getType()) {
				case Combinaison.TypeCombinaison.FULL_HOUSE:
				case Combinaison.TypeCombinaison.DEUX_PAIRES:
					if ((a.getValeurs().get(0) + 11) % 13 > (b.getValeurs().get(0) + 11) % 13) {
						return a;
					} else if ((a.getValeurs().get(0) + 11) % 13 < (b.getValeurs().get(0) + 11) % 13) {
							return b;
					} else {
						if ((a.getValeurs().get(1) + 11) % 13 > (b.getValeurs().get(1) + 11) % 13) {
							return a;
						} else if ((a.getValeurs().get(1) + 11) % 13 < (b.getValeurs().get(1) + 11) % 13) {
							return b;
						}
					}
					break;
				case Combinaison.TypeCombinaison.QUINTE_FLUSH:
				case Combinaison.TypeCombinaison.CARRE:
				case Combinaison.TypeCombinaison.QUINTE:
				case Combinaison.TypeCombinaison.BRELAN:
				case Combinaison.TypeCombinaison.PAIRE:
				case Combinaison.TypeCombinaison.CARTE_HAUTE:
					if ((a.getValeurs().get(0) + 11) % 13 > (b.getValeurs().get(0) + 11) % 13) {
						return a;
					} else if ((a.getValeurs().get(0) + 11) % 13 < (b.getValeurs().get(0) + 11) % 13) {
							return b;
					}
					break;
			}
			return null;
		}
	}
}
