package edu.info0502.tp4;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import org.json.JSONObject;

import java.util.ArrayList;

public class App {
	final static String HOST = "10.11.18.162";
	final static String LOG = "bruh";
	final static String PASS = "bruh";

	final static String USER_QUEUE = "user";
	final static String QCM_QUEUE = "qcm";

	public static void main(String[] args) {
		new Server().start();
		new Client().start();

		/*
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		factory.setUsername(LOG);
		factory.setPassword(PASS);

		new Thread(() -> {
			try {
				Connection connection = factory.newConnection();
				Channel channel = connection.createChannel();

				JSONObject inscription = new JSONObject();
				inscription.put("pseudo", "titouan");
				inscription.put("op", "creation");

				channel.basicPublish("", USER_QUEUE, null, inscription.toString().getBytes());

				DeliverCallback qcmDeliverCallback = (consumerTag, delivery) -> {
					String json = new String(delivery.getBody(), "UTF-8");
					JSONObject qcm = new JSONObject(json);

					System.out.println(qcm);
				};

				channel.queueDeclare(QCM_QUEUE + "titouan", false, false, false, null);

				JSONObject demande_qcm = new JSONObject();
				demande_qcm.put("pseudo", "titouan");

				channel.basicPublish("", QCM_QUEUE, null, demande_qcm.toString().getBytes());

				channel.basicConsume(QCM_QUEUE + "titouan", true, qcmDeliverCallback, consumerTag -> {});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();

		new Thread(() -> {
			try {
				Connection connection = factory.newConnection();
				Channel channel = connection.createChannel();

				channel.queueDeclare(USER_QUEUE, false, false, false, null);
				channel.queueDeclare(QCM_QUEUE, false, false, false, null);

				ArrayList<String> utilisateurs = new ArrayList<>();

				DeliverCallback userDeliverCallback = (consumerTag, delivery) -> {
					String json = new String(delivery.getBody(), "UTF-8");
					JSONObject inscription = new JSONObject(json);

					if (inscription.getString("op").equals("creation")) {
						System.out.println("Nouveau utilisateur: " + inscription.getString("pseudo"));

						if (!utilisateurs.contains(inscription.getString("pseudo"))) {
							utilisateurs.add(inscription.getString("pseudo"));
						}
					} else if (inscription.getString("op").equals("supprimer")) {
						System.out.println("Suppression de l'utilisateur: " + inscription.getString("pseudo"));

					}
				};

				DeliverCallback demande_qcmDeliverCallback = (consumerTag, delivery) -> {
					String json = new String(delivery.getBody(), "UTF-8");
					JSONObject demande_qcm = new JSONObject(json);

					if (utilisateurs.contains(demande_qcm.getString("pseudo"))) {
						JSONObject qcm = new JSONObject();
						qcm.put("titre", "qcm test");

						channel.basicPublish("", QCM_QUEUE + demande_qcm.getString("pseudo"), null, qcm.toString().getBytes());
					}
				};

				channel.basicConsume(USER_QUEUE, true, userDeliverCallback, consumerTag -> {});
				channel.basicConsume(QCM_QUEUE, true, demande_qcmDeliverCallback, consumerTag -> {});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	*/
	}
}
