package edu.info0502.tp4;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.ArrayList;

public class Qcm {
	static JSONArray qcm_json = generateJsonAndResponses();
	static int responses[] = { 3, 1 };

	static JSONArray generateJsonAndResponses() {
		JSONArray json = new JSONArray();

		JSONObject question1 = new JSONObject();
		question1.put("text", "Combient font 2 + 2 ?");

		JSONArray question1_proprositions = new JSONArray();
		question1.put("propositions", question1_proprositions);

		question1_proprositions.put("1");
		question1_proprositions.put("3");
		question1_proprositions.put("6");
		question1_proprositions.put("2");

		json.put(question1);


		JSONObject question2 = new JSONObject();
		question2.put("text", "Combient font 2 - 2 ?");

		JSONArray question2_proprositions = new JSONArray();
		question2.put("propositions", question1_proprositions);

		question2_proprositions.put("1");
		question2_proprositions.put("0");
		question2_proprositions.put("6");
		question2_proprositions.put("2");

		json.put(question2);

		return json;
	}

	public static JSONArray getJson() {
		return qcm_json;
	}

	public static int[] getResponses() {
		return responses;
	}
}
