package edu.info0502.tp4;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Delivery;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.Scanner;

public class Client extends Thread {
	final static String HOST = "10.11.18.162";
	final static String LOG = "bruh";
	final static String PASS = "bruh";

	final static String pseudo = "Titouan";

	static Channel channel = null;

	public void run() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		factory.setUsername(LOG);
		factory.setPassword(PASS);

		try {
			Connection connection = factory.newConnection();
			channel = connection.createChannel();

			channel.queueDeclare(Queue.QCM + pseudo, false, false, false, null);
			channel.basicConsume(Queue.QCM + pseudo, true, Client::qcm_reception_callback, consumerTag -> {});

			channel.queueDeclare(Queue.QCM_RESULT + pseudo, false, false, false, null);
			channel.basicConsume(Queue.QCM_RESULT + pseudo, true, Client::qcm_result_callback, consumerTag -> {});

			channel.queueDeclare(Queue.SCORE_RESPONSE + pseudo, false, false, false, null);
			channel.basicConsume(Queue.SCORE_RESPONSE + pseudo, true, Client::score_request_callback, consumerTag -> {});

			JSONObject inscription = new JSONObject();
			inscription.put("pseudo", pseudo);
			inscription.put("op", "inscription");

			channel.basicPublish("", Queue.USER_MANAGEMENT, null, inscription.toString().getBytes());
			Thread.sleep(1000);


			JSONObject demande_qcm = new JSONObject();
			demande_qcm.put("pseudo", pseudo);

			channel.basicPublish("", Queue.QCM_REQUEST, null, demande_qcm.toString().getBytes());
			Thread.sleep(1000);


			JSONObject demande_score = new JSONObject();
			demande_score.put("pseudo", pseudo);

			channel.basicPublish("", Queue.SCORE_REQUEST, null, demande_score.toString().getBytes());


			Thread.sleep(3000);


			JSONObject desinscription = new JSONObject();
			desinscription.put("pseudo", pseudo);
			desinscription.put("op", "desinscription");

			channel.basicPublish("", Queue.USER_MANAGEMENT, null, desinscription.toString().getBytes());

			while (true) {}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void qcm_reception_callback(String consumer_tag, Delivery delivery) {
		String json_string = new String(delivery.getBody());
		JSONArray json = new JSONArray(json_string);

		System.out.println("Qcm received json : " + json_string);

		JSONObject qcm_response = new JSONObject();
		qcm_response.put("pseudo", pseudo);

		JSONArray responses = new JSONArray();
		qcm_response.put("responses", responses);

		for (int i = 0; i < json.length(); i++) {
			JSONObject question = json.getJSONObject(i);
			JSONArray propositions = question.getJSONArray("propositions");

			System.out.println(question.getString("text"));

			for (int j = 0; j < propositions.length(); j++) {
				System.out.println(j + ": " + propositions.getString(j));
			}

			System.out.println("Réponse choisie : 1");
			responses.put(1);
		}

		try {
			channel.basicPublish("", Queue.QCM_RESPONSE, null, qcm_response.toString().getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void qcm_result_callback(String consumer_tag, Delivery delivery) {
		String json_string = new String(delivery.getBody());
		JSONObject json = new JSONObject(json_string);

		System.out.println("Qcm result json : " + json_string);
		System.out.println("Score : " + json.getInt("score"));
	}

	public static void score_request_callback(String consumer_tag, Delivery delivery) {
		String json_string = new String(delivery.getBody());
		JSONObject json = new JSONObject(json_string);

		System.out.println("Score json : " + json_string);
		System.out.println("Score : " + json.getInt("score"));
	}
}
