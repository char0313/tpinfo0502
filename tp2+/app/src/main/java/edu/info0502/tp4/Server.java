package edu.info0502.tp4;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Delivery;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.Hashtable;

public class Server extends Thread {
	final static String HOST = "10.11.18.162";
	final static String LOG = "bruh";
	final static String PASS = "bruh";

	static Channel channel = null;

	static Hashtable<String, Integer> users = new Hashtable<>();

	public void run() {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		factory.setUsername(LOG);
		factory.setPassword(PASS);

		try {
			Connection connection = factory.newConnection();
			channel = connection.createChannel();

			channel.queueDeclare(Queue.USER_MANAGEMENT, false, false, false, null);
			channel.basicConsume(Queue.USER_MANAGEMENT, true, Server::user_management_callback, consumerTag -> {});

			channel.queueDeclare(Queue.QCM_REQUEST, false, false, false, null);
			channel.basicConsume(Queue.QCM_REQUEST, true, Server::qcm_request_callback, consumerTag -> {});

			channel.queueDeclare(Queue.QCM_RESPONSE, false, false, false, null);
			channel.basicConsume(Queue.QCM_RESPONSE, true, Server::qcm_response_callback, consumerTag -> {});

			channel.queueDeclare(Queue.SCORE_REQUEST, false, false, false, null);
			channel.basicConsume(Queue.SCORE_REQUEST, true, Server::score_request_callback, consumerTag -> {});

			while (true) {}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void user_management_callback(String consumer_tag, Delivery delivery) {
		String json_string = new String(delivery.getBody());
		JSONObject json = new JSONObject(json_string);

		System.out.println("User management json : " + json_string);

		if (json.getString("op") != null
				&& json.getString("op").equals("inscription")
				&& json.getString("pseudo") != null
				&& !users.containsKey(json.getString("pseudo"))) {
			users.put(json.getString("pseudo"), 0);
		} else if (json.getString("op") != null
				&& json.getString("op").equals("desinscription")
				&& json.getString("pseudo") != null
				&& users.containsKey(json.getString("pseudo"))) {
			users.remove(json.getString("pseudo"));
		}

		System.out.println(users);
	}

	public static void qcm_request_callback(String consumer_tag, Delivery delivery) {
		String json_string = new String(delivery.getBody());
		JSONObject json = new JSONObject(json_string);

		System.out.println("Qcm request json : " + json_string);

		if (json.getString("pseudo") != null
				&& users.containsKey(json.getString("pseudo"))) {
			try {
				channel.basicPublish("", Queue.QCM + json.getString("pseudo"), null, Qcm.getJson().toString().getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void qcm_response_callback(String consumer_tag, Delivery delivery) {
		String json_string = new String(delivery.getBody());
		JSONObject json = new JSONObject(json_string);

		System.out.println("Qcm response json : " + json_string);

		if (json.getString("pseudo") != null
				&& users.containsKey(json.getString("pseudo"))
				&& json.getJSONArray("responses") != null
				&& json.getJSONArray("responses").length() == Qcm.getResponses().length) {
			JSONArray responses = json.getJSONArray("responses");

			int score = 0;
			for (int i = 0; i < responses.length(); i++) {
				if (responses.getInt(i) == Qcm.getResponses()[i]) {
					score += 1;
				}
			}

			if (score > users.get(json.getString("pseudo"))) {
				users.put(json.getString("pseudo"), score);
			}

			JSONObject result = new JSONObject();
			result.put("score", users.get(json.getString("pseudo")));

			try {
				channel.basicPublish("", Queue.QCM_RESULT + json.getString("pseudo"), null, result.toString().getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void score_request_callback(String consumer_tag, Delivery delivery) {
		String json_string = new String(delivery.getBody());
		JSONObject json = new JSONObject(json_string);

		System.out.println("Score request json : " + json_string);

		if (json.getString("pseudo") != null
				&& users.containsKey(json.getString("pseudo"))) {

			JSONObject result = new JSONObject();
			result.put("score", users.get(json.getString("pseudo")));

			try {
				channel.basicPublish("", Queue.SCORE_RESPONSE + json.getString("pseudo"), null, result.toString().getBytes());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
