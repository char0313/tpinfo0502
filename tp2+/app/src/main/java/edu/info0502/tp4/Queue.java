package edu.info0502.tp4;

public class Queue {
	public static final String USER_MANAGEMENT = "USER_MANAGEMENT";

	public static final String QCM_REQUEST = "QCM_REQUEST";
	public static final String QCM = "QCM";
	public static final String QCM_RESPONSE = "QCM_RESPONSE";
	public static final String QCM_RESULT = "QCM_RESULT";

	public static final String SCORE_REQUEST = "SCORE_REQUEST";
	public static final String SCORE_RESPONSE = "SCORE_RESPONSE";
}
