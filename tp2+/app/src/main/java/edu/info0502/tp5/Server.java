package edu.info0502.tp5;

import edu.info0502.tp2.*;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;

import org.json.JSONObject;
import org.json.JSONArray;

import java.util.Hashtable;
import java.util.ArrayList;

public class Server extends Thread {
	public static final String HOST = "tcp://10.11.18.162:1883";
	public static final String CLIENT_ID = "SERVER";

	static Hashtable<String, ArrayList<String>> parties = new Hashtable<>();

	static MqttClient client = null;

	public void run() {
		try {
			client = new MqttClient(HOST, CLIENT_ID);
			MqttConnectOptions connect_options = new MqttConnectOptions();
			connect_options.setCleanSession(true);

			client.setCallback(new ServerCallback());
			client.connect(connect_options);

			client.subscribe(Topic.PARTIE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static class ServerCallback implements MqttCallback {
		public void messageArrived(String topic, MqttMessage message) throws Exception {
			String json_string = new String(message.getPayload());
			JSONObject json = new JSONObject(json_string);

			//System.out.println("New message json: " + json_string);

			if (topic.equals(Topic.PARTIE)) {
				if (json.getString("pseudo") != null
						&& json.getString("op") != null
						&& json.getString("partie") != null) {

					if (json.getString("op").equals("creation")) {
						parties.put(json.getString("partie"), new ArrayList<>());
						parties.get(json.getString("partie")).add(json.getString("pseudo"));

						System.out.println("Nouvelle table: " + json.getString("partie"));

					} else if (json.getString("op").equals("rejoindre")) {
						if (parties.get(json.getString("partie")).size() < 8) {
							parties.get(json.getString("partie")).add(json.getString("pseudo"));
							
							System.out.println(json.getString("pseudo") + " rejoins la table " + json.getString("partie"));
						}

					} else if (json.getString("op").equals("quitter")) {
						if (parties.get(json.getString("partie")).indexOf(json.getString("pseudo")) == 0) {
							parties.remove(json.getString("partie"));

						} else {
							parties.get(json.getString("partie")).remove(json.getString("pseudo"));

						}

						System.out.println(json.getString("pseudo") + " quitte la table " + json.getString("partie"));

					} else if (json.getString("op").equals("commencer")) {
						System.out.println("Lancement de la table: " + json.getString("partie"));

						ArrayList<String> joueurs = parties.get(json.getString("partie"));

						Talon talon = new Talon(4);
						talon.melanger();

						Carte[][] mains = new Carte[joueurs.size()][];

						for (int i = 0; i < joueurs.size(); i++) {
							mains[i] = talon.getCartes(2);
						}

						Carte[] flop = talon.getCartes(3);
						Carte turn = talon.getCarte();
						Carte river = talon.getCarte();


						MainHelper.Combinaison combinaison_gagnante = null;
						String pseudo_gagnant = null;
						MainHelper.Combinaison combinaisons[] = new MainHelper.Combinaison[joueurs.size()];
						for (int i = 0; i < joueurs.size(); i++) {
							combinaisons[i] = MainHelper.evaluer(mains[i], flop, turn, river);
							if (combinaison_gagnante == null) {
								combinaison_gagnante = combinaisons[i];
								pseudo_gagnant = joueurs.get(i);
							} else {
								combinaison_gagnante = MainHelper.comparer(combinaison_gagnante, combinaisons[i]);
								pseudo_gagnant = joueurs.get(i);
							}
						}

						JSONObject table = new JSONObject();
						table.put("type", "table");

						JSONArray table_flop = new JSONArray();
						table.put("flop", table_flop);
						table_flop.put(flop[0].getJSONObject());
						table_flop.put(flop[1].getJSONObject());
						table_flop.put(flop[2].getJSONObject());

						table.put("turn", turn.getJSONObject());
						table.put("river", river.getJSONObject());


						client.publish(json.getString("partie"), new MqttMessage(table.toString().getBytes()));

						for (int i = 0; i < joueurs.size(); i++) {
							JSONObject main = new JSONObject();
							main.put("type", "main");
							main.put("pseudo", joueurs.get(i));

							JSONArray cartes = new JSONArray();
							main.put("cartes", cartes);
							cartes.put(mains[i][0].getJSONObject());
							cartes.put(mains[i][1].getJSONObject());

							client.publish(json.getString("partie"), new MqttMessage(main.toString().getBytes()));
						}

						JSONObject gagnant = new JSONObject();
						gagnant.put("type", "gagnant");
						gagnant.put("pseudo", pseudo_gagnant);

						client.publish(json.getString("partie"), new MqttMessage(gagnant.toString().getBytes()));
					}
				}
			}
		}

		public void connectionLost(Throwable cause) {
			System.out.println("Connexion lost: " + cause.getMessage());
		}

		public void deliveryComplete(IMqttDeliveryToken token) {}
	}
}
