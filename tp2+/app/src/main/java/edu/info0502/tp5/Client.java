package edu.info0502.tp5;

import edu.info0502.tp2.*;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;

import org.json.JSONObject;
import org.json.JSONArray;

public class Client {
	public static final String HOST = "tcp://10.11.18.162:1883";

	String pseudo;
	MqttClient client;

	public Client(String pseudo) throws Exception {
		this.pseudo = pseudo;

		client = new MqttClient(HOST, pseudo);
		
		MqttConnectOptions connect_options = new MqttConnectOptions();
		connect_options.setCleanSession(true);

		client.setCallback(new ClientCallback());
		client.connect(connect_options);
	}

	public void creerPartie(String partie) throws Exception {
		JSONObject creation = new JSONObject();
		creation.put("pseudo", pseudo);
		creation.put("partie", partie);
		creation.put("op", "creation");

		client.publish(Topic.PARTIE, new MqttMessage(creation.toString().getBytes()));
		client.subscribe(partie);
	}

	public void rejoindrePartie(String partie) throws Exception {
		JSONObject rejoindre = new JSONObject();
		rejoindre.put("pseudo", pseudo);
		rejoindre.put("partie", partie);
		rejoindre.put("op", "rejoindre");

		client.publish(Topic.PARTIE, new MqttMessage(rejoindre.toString().getBytes()));
		client.subscribe(partie);
	}

	public void quitterPartie(String partie) throws Exception {
		JSONObject quitter = new JSONObject();
		quitter.put("pseudo", pseudo);
		quitter.put("partie", partie);
		quitter.put("op", "quitter");

		client.publish(Topic.PARTIE, new MqttMessage(quitter.toString().getBytes()));
		client.unsubscribe(partie);
	}

	public void commencerPartie(String partie) throws Exception {
		JSONObject commencer = new JSONObject();
		commencer.put("pseudo", pseudo);
		commencer.put("partie", partie);
		commencer.put("op", "commencer");

		client.publish(Topic.PARTIE, new MqttMessage(commencer.toString().getBytes()));
	}

	class ClientCallback implements MqttCallback {
		public void messageArrived(String topic, MqttMessage message) throws Exception {
			String json_string = new String(message.getPayload());
			JSONObject json = new JSONObject(json_string);

			//System.out.println("New message json: " + json_string);

			if (json.getString("type").equals("main")) {
				JSONArray cartes = json.getJSONArray("cartes");
				Carte a = new Carte(cartes.getJSONObject(0));
				Carte b = new Carte(cartes.getJSONObject(1));

				if (!json.getString("pseudo").equals(pseudo)) {
					System.out.println("De " + pseudo + ": main de " + json.getString("pseudo") + ": " + a + ", " + b);
				} else {
					System.out.println("De " + pseudo + ": ma main " + ": " + a + ", " + b);
				}
			} else if (json.getString("type").equals("table")) {
				JSONArray flop = json.getJSONArray("flop");
				Carte a = new Carte(flop.getJSONObject(0));
				Carte b = new Carte(flop.getJSONObject(1));
				Carte c = new Carte(flop.getJSONObject(2));

				Carte turn = new Carte(json.getJSONObject("turn"));
				Carte river = new Carte(json.getJSONObject("river"));

				System.out.println("De " + pseudo + ": flop: " + a + ", " + b + ", " + c + " turn: " + turn + " river: " + river);
			} else if (json.getString("type").equals("gagnant")) {
				System.out.println("De " + pseudo + ": gagnant de la partie: " + json.getString("pseudo"));
			}
		}

		public void connectionLost(Throwable cause) {
			System.out.println("Connection lost: " + cause.getMessage());
		}

		public void deliveryComplete(IMqttDeliveryToken token) {}
	}
}
