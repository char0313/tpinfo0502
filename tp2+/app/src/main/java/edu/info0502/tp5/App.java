package edu.info0502.tp5;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;

public class App {
	public static final String HOST = "tcp://10.11.18.162:1883";
	public static final String TOPIC = "TEST";

	public static void main(String args[]) {
		new Server().start();

		try {
			Client titouan = new Client("Titouan");
			titouan.creerPartie("partie de titouan");


			Client enzo = new Client("Enzo");
			enzo.creerPartie("partie de enzo");

			Client pierre = new Client("Pierre");
			pierre.rejoindrePartie("partie de titouan");
			pierre.quitterPartie("partie de titouan");
			pierre.rejoindrePartie("partie de enzo");

			Client claire = new Client("Claire");
			claire.rejoindrePartie("partie de titouan");

			titouan.commencerPartie("partie de titouan");
			enzo.commencerPartie("partie de enzo");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static class Callback implements MqttCallback {
		public void messageArrived(String topic, MqttMessage message) throws Exception {
			System.out.println("Message reçu topic: " + topic + " message: " + new String(message.getPayload()));
		}

		public void connectionLost(Throwable cause) {
			System.out.println("Connexion perdue: " + cause.getMessage());
		}

		public void deliveryComplete(IMqttDeliveryToken token) {}
	}

	void test() {
		new Thread(() -> {
			try {
				MqttClient client = new MqttClient(HOST, "publisher");
				client.connect();

				MqttMessage message = new MqttMessage("hello".getBytes());

				System.out.println("Evoie d'un message");
				client.publish(TOPIC, message);

				client.disconnect();
				client.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();

		new Thread(() -> {
			try {
				MqttClient client = new MqttClient(HOST, "subscriber");
				MqttConnectOptions connect_options = new MqttConnectOptions();
				connect_options.setCleanSession(true);

				client.setCallback(new Callback());
				client.connect(connect_options);
				client.subscribe(TOPIC);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();

		while (true) {}
	}
}
