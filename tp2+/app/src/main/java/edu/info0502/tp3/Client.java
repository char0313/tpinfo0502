package edu.info0502.tp3;

import edu.info0502.tp2.*;

import java.net.Socket;
import java.net.InetSocketAddress;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

public class Client {
	public static void main(String[] args) {
		try {
			System.out.println("Connection");
			Socket socket = new Socket("10.11.18.162", 9999);
			System.out.println("Connecté");

			ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

			oos.writeObject("titouan");
			oos.flush();

			String reponse = (String) ois.readObject();
			System.out.println(reponse);

			while (true) {
				oos.writeObject("lancer partie");
				oos.flush();

				reponse = (String) ois.readObject();

				if (reponse.equals("partie commence")) {
					break;
				} else {
					System.out.println(reponse);
				}

				Thread.sleep(10000);
			}

			System.out.println("Début de la partie");

			Carte[] main = (Carte[]) ois.readObject();
			
			System.out.println("Main reçue : " + main[0].toString() + ", " + main[1].toString());
			
			Carte[] flop = (Carte[]) ois.readObject();
			System.out.print("Flop : ");
			for (Carte carte: flop) {
				System.out.print(carte.toString() + ", ");
			}
			System.out.println();

			Carte turn = (Carte) ois.readObject();
			System.out.println("Turn : " + turn.toString());

			Carte river = (Carte) ois.readObject();
			System.out.println("River : " + river.toString());

			MainHelper.Combinaison combinaison = MainHelper.evaluer(main, flop, turn, river);
			System.out.println("Combinaison : " + combinaison.toString());

			System.out.println("Main des autres joueurs :");

			reponse = (String) ois.readObject();
			while (!reponse.equals("fin des mains")) {
				System.out.println(reponse);
				reponse = (String) ois.readObject();
			}

			String gagnant = (String) ois.readObject();
			System.out.println("Gagnant: " + gagnant);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
