package edu.info0502.tp3;

import edu.info0502.tp2.*;

import java.net.ServerSocket;
import java.net.Socket;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

import java.util.ArrayList;

public class Server {
	static class Joueur {
		Socket socket;
		public ObjectOutputStream oos;
		public ObjectInputStream ois;

		String pseudo;

		public Joueur(Socket socket) throws Exception {
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());

			String pseudo = (String) ois.readObject();

			for (Joueur joueur: joueurs) {
				if (joueur.pseudo.equals(pseudo)) {
					oos.writeObject("Pseudo déjà utilisé.");
					oos.flush();
					
					throw new Exception("Pseudo déjà utilisé.");
				}
			}

			oos.writeObject("Bienvenue dans la partie de poker.");
			oos.flush();

			this.socket = socket;
			this.pseudo = pseudo;
		}

		public String getPseudo() { return pseudo; }
	}

	static class Partie {
		int nombre_joueurs;
		Talon talon;

		public Partie(int nombre_joueurs) {
			this.nombre_joueurs = nombre_joueurs;

			talon = new Talon(4);
			talon.melanger();
		}

		public Carte[][] getMainsJoueurs() {
			Carte[][] cartes = new Carte[nombre_joueurs][2];

			for (Carte[] main: cartes) {
				main[0] = talon.getCarte();
				main[1] = talon.getCarte();
			}

			return cartes;
		}
	}

	static ArrayList<Joueur> joueurs = new ArrayList<>();
	static boolean partie_commence = false;

	public static void main(String[] args) {
		ServerSocket socket;

		try {
			socket = new ServerSocket(9999);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		while (true) {
			try {
				Socket client = socket.accept();
				System.out.println("Nouvelle connection.");

				new Thread(() -> {
					try {
						Joueur joueur = new Joueur(client);
						synchronized (joueurs) {
							joueurs.add(joueur);
						}

						System.out.println("Nouveau joueur " + joueur.pseudo + ".");

						while (true) {
							try {
								String requete = (String) joueur.ois.readObject();

								if (requete.equals("lancer partie")) {
									synchronized (joueurs) {
										if (partie_commence) {
											joueur.oos.writeObject("partie commence");
											joueur.oos.flush();
										} else {
											if (joueurs.size() >= 2) {
												joueur.oos.writeObject("partie commence");
												joueur.oos.flush();

												partie_commence = true;

												Talon talon = new Talon(4);
												talon.melanger();
												
												Carte[][] mains = new Carte[joueurs.size()][];

												for (int i = 0; i < joueurs.size(); i++) {
													mains[i] = talon.getCartes(2);

													joueurs.get(i).oos.writeObject(mains[i]);
													joueurs.get(i).oos.flush();
												}

												// Tirage du flop
												Carte[] flop = talon.getCartes(3);
												// Tirage du turn
												Carte turn = talon.getCarte();
												// Tirage du river
												Carte river = talon.getCarte();

												for (int i = 0; i < joueurs.size(); i++) {
													joueurs.get(i).oos.writeObject(flop);
													joueurs.get(i).oos.writeObject(turn);
													joueurs.get(i).oos.writeObject(river);
													joueurs.get(i).oos.flush();
												}

												// Evaluation des mains
												MainHelper.Combinaison combinaison_gagnante = null;
												MainHelper.Combinaison combinaisons[] = new MainHelper.Combinaison[joueurs.size()];
												for (int i = 0; i < joueurs.size(); i++) {
													combinaisons[i] = MainHelper.evaluer(mains[i], flop, turn, river);
													if (combinaison_gagnante == null) {
														combinaison_gagnante = combinaisons[i];
													} else {
														combinaison_gagnante = MainHelper.comparer(combinaison_gagnante, combinaisons[i]);
													}
												}

												// Envoie des mains
												for (int i = 0; i < joueurs.size(); i++) {
													for (int j = 0; j < joueurs.size(); j++) {
														if (joueurs.get(i) != joueurs.get(j)) {
															joueurs.get(i).oos.writeObject(joueurs.get(j).getPseudo() + ": " + MainHelper.mainToString(combinaisons[j].getMain()));
															joueurs.get(i).oos.flush();
														}
													}
												}

												for (int i = 0; i < joueurs.size(); i++) {
													joueurs.get(i).oos.writeObject("fin des mains");
													joueurs.get(i).oos.flush();
												}

												// Envoie du gagnant
												for (int i = 0; i < joueurs.size(); i++) {
													if (combinaisons[i] == combinaison_gagnante) {
														for (int j = 0; j < joueurs.size(); j++) {
															joueurs.get(j).oos.writeObject(joueurs.get(i).getPseudo());
															joueurs.get(j).oos.flush();
														}
														break;
													}
												}
											} else {
												joueur.oos.writeObject("pas asser de joueurs");
												joueur.oos.flush();
											}
										}
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								break;
							}
						}

						synchronized (joueurs) {
							joueurs.remove(joueur);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}).start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
