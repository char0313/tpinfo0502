public class Livre extends Media {
	String auteur;
	String isbn;

	Livre() {
		super();
		super.setNom("Livre");
		auteur = "auteur par défaut";
		isbn = "isbn par défaut";
	}

	Livre(String titre, String cote, int note, String auteur, String isbn) {
		super(titre, cote, note);
		super.setNom("Livre");
		this.auteur = auteur;
		this.isbn = isbn;
	}

	Livre(Livre other) {
		super(other);
		super.setNom("Livre");
		auteur = other.auteur;
		isbn = other.isbn;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException("Clonage non supporté.");
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || obj instanceof Livre) return false;
		Livre livre = (Livre) obj;
		return super.equals(obj) &&
			auteur.equals(livre.auteur) &&
			isbn.equals(livre.isbn);
	}

	@Override
	public String toString() {
		return super.toString() + ", Auteur: " + auteur + ", Isbn: " + isbn;
	}
}
