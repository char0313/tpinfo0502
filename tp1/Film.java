public class Film extends Media {
	String realisateur;
	int annee;

	Film() {
		super();
		super.setNom("Film");
		realisateur = "Réalisateur par défaut";
		annee = 0;
	}

	Film(String titre, String cote, int note, String realisateur, int annee) {
		super(titre, cote, note);
		super.setNom("Film");
		this.realisateur = realisateur;
		this.annee = annee;
	}

	Film(Film other) {
		super(other);
		super.setNom("Livre");
		realisateur = other.realisateur;
		annee = other.annee;
	}

	public String getRealisateur() {
		return realisateur;
	}

	public void setRealisateur(String realisateur) {
		this.realisateur = realisateur;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException("Clonage non supporté.");
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || obj instanceof Film) return false;
		Film film = (Film) obj;
		return super.equals(obj) &&
			realisateur.equals(film.realisateur) &&
			annee == film.annee;
	}

	@Override
	public String toString() {
		return super.toString() + ", Réalisateur: " + realisateur + ", Année: " + annee;
	}
}
