import java.util.Vector;

public class Mediatheque {
	String proprietaire;
	Vector<Media> medias;

	public static void main(String[] args) {
		Mediatheque m = new Mediatheque();
		m.setProprietaire("Charrier");
		System.out.println(m);

		Media m1 = new Media("Kill Bill vol 1","2BXX5",5);
		Media m2 = new Livre("Le livre de la jungle","DFTY4HG",5,"R. Kippling","2081263246");
		Media m3 = new Film("Titanic", "ABCD", 1, "Moi", 2004);

		m.add(m1);
		m.add(m2);
		m.add(m3);

		System.out.println(m);
	}

	Mediatheque() {
		proprietaire = "Propriétaire par défaut";
		medias = new Vector<Media>();
	}

	Mediatheque(Mediatheque other) {
		proprietaire = other.proprietaire;
		medias = new Vector<Media>();
		for (Media media: other.medias) {
			if (media instanceof Livre) {
				Livre l = (Livre)media;
				medias.addElement(new Livre(l));
			} else if (media instanceof Film) {
				Film f = (Film)media;
				medias.addElement(new Film(f));
			}
		}
	}

	public String getProprietaire() {
		return proprietaire;
	}

	public void setProprietaire(String proprietaire) {
		this.proprietaire = proprietaire;
	}

	public void add(Media media) {
		medias.addElement(media);
	}

	@Override
	public String toString() {
		String repr = "Propriétaire: " + proprietaire + "\n";
		for (Media media: medias) {
			repr = repr + media.toString() + "\n";
		}

		return repr;
	}
}
