public class Media {
	String titre;
	StringBuffer cote;
	int note;

	static String nom;

	public static void main(String[] args) {
		Media m1 = new Media("Kill Bill vol 1","2BXX5",5);
		System.out.println(m1);
		m1.getCote().reverse();
		System.out.println(m1);

		Media m2 = new Livre("Le livre de la jungle","DFTY4HG",5,"R. Kippling","2081263246");
		System.out.println(m2);
		m2.setCote("SD77DS");
		//System.out.println(m2.getAuteur());
		System.out.println(((Livre)m2).getAuteur());
		System.out.println(m2.getNom());

		Media m3 = new Film("Titanic", "ABCD", 1, "Moi", 2004);
		System.out.println(m3);
	}

	Media() {
		titre = "titre par défaut";
		cote = new StringBuffer("cote par défaut");
		note = -1;
	}

	Media(String titre, String cote, int note) {
		this.titre = titre;
		this.cote = new StringBuffer(cote);
		this.note = note;
	}

	Media(Media other) {
		titre = other.titre;
		cote = other.cote;
		note = other.note;
	}

	public String getTitre() {
		return titre;
	}

	public StringBuffer getCote() {
		return new StringBuffer(cote);
	}

	public int getNote() {
		return note;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setCote(String cote) {
		this.cote = new StringBuffer(cote);
	}

	public void setNote(int note) {
		this.note = note;
	}

	public static void setNom(String nouveau_nom) {
		nom = nouveau_nom;
	}

	public static String getNom() {
		return nom;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException("Clonage non supporté.");
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null || obj instanceof Media) return false;
		Media media = (Media) obj;
		return note == media.note &&
			titre.equals(media.titre) &&
			cote.toString().equals(media.cote.toString());
	}

	@Override
	public String toString() {
		return "Titre: " + titre + ", Cote: " + cote + ", Note: " + note;
	}
}
